# language: ru
@all @ui @globheader @itemscategory @streetwear
Функция: При наведении курсора на Streetwear появляется список с товарами для мужчин/женщин/молодежи

  @success
  Сценарий: Навести курсор на Streetwear, появляется список с товарами для MEN/WOMEN/YOUTH
    Дано открыть главную страницу
    Когда навести курсор на "Streetwear"
    Тогда появляется список с товарами для
      | MEN          |
      | WOMEN |
      | YOUTH   |