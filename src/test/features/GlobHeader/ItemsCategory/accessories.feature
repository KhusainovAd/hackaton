# language: ru
@all @brands @ui @globheader @accessories
Функция: При наведении курсора на Accessories появляется список с товарами для мужчин/женщин + кнопки Browse All

  @success
  Сценарий: Навести курсор на Accessories, появляется список с товарами для MEN/WOMEN + кнопки Browse All
    Дано открыть главную страницу
    Когда навести курсор на "Accessories"
    Тогда появляется список с товарами для
      | MEN |
      | WOMEN |
    Тогда появляется кнопки "Browse All"