import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import java.util.logging.Logger;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features",
        glue = "tests.hackaton",
        dryRun = false,
        snippets = CucumberOptions.SnippetType.CAMELCASE
//      name = "^Успешное|Успешная.*"
)
public class RunnerTest {
    private static Logger logger = Logger.getLogger(RunnerTest.class.getName());
}