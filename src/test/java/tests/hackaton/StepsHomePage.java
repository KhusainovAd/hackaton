package tests.hackaton;

import io.cucumber.java.PendingException;
import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageObject.HomePage;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class StepsHomePage extends DriverFactory {
    private static Logger logger = Logger.getLogger(StepsHomePage.class.getName());
    private DriverFactory driverFactory;
    private String titleOfCollection;
    private String subTitleOfCollection;
    private List<WebElement> listOfElements;
    ArrayList<String> listOfSubTitles;
    HomePage homePage;

    @Дано("открыть главную страницу")
    public void открытьГлануюСтраницу() throws PendingException {
        homePage = new HomePage();
/*        logger.info("Открываю главную страницу:" + System.getProperty("mainpage"));
        driver.get(System.getProperty("mainpage"));
        Assert.assertEquals("Apparel Site UK | Homepage", driver.getTitle());*/
    }

    @Дано("Получить список баннеров на странице")
    public void получитьСписокБаннеровНаСтранице() {
        homePage.getTitlesOfBunnersOnMainPage();
    }

    @Когда("Получить список товаров на странице")
    public void получитьСписокТоваровНаСтранице() {
        homePage.getListOfCarouselItems();
    }

    @Когда("Пользователь кликает по следующему товару выполняется переход на страницу соответствующую нажатому товару")
    public void пользовательКликаетПоСледующемуТовару() {
        homePage.clickOnNextCarouselItemCheckOpenedPageThenReturnToMainPage();
    }

    @Дано("Пользователь на главной странице")
    public void пользовательНаГлавнойСтранице() {
    }

    @Когда("Пользователь кликает по баннеру: Зимняя Коллекция")
    public void пользовательКликаетПоБаннеруЗимняяКоллекция() {
        driver.findElement(By.xpath("//*[@title = 'Start Your Season']")).click();
    }

    @Когда("Пользователь кликает по следующему баннеру выполняется переход на страницу соответствующую нажатому банеру")
    public void пользовательКликаетПоСледующемуБаннеру() {
        homePage.clickOnNextBannerCheckOpenedPageThenReturnToMainPage();
    }

    @Тогда("выполняется переход на страницу баннера")
    public void выполняетсяПереходНаСтраницу() {
        Assert.assertEquals("Snow | Collections | Apparel Site UK", driver.getTitle());
    }

    @Тогда("выполняется переход на страницу Зимней коллекции")
    public void выполняетсяПереходНаСтраницуЗимнейКоллекции() {
        Assert.assertEquals("Snow | Collections | Apparel Site UK", driver.getTitle());
    }

    @Тогда("не выполняется переход на новую страницу")
    public void неВыполняетсяПереходНаНовуюСтраницу() {
    }


    @Дано("Проверить наличие куки")
    public void проверитьНаличиеКуки() {
        Assert.assertTrue(homePage.checkCookiePresented());
    }

    @Когда("Пользователь кликает на закрыть куки")
    public void пользовательКликаетНаЗакрытьКуки() {
        homePage.closeCookiesMsg();
    }

    @Тогда("Сообщение о куки отсутствует")
    public void сообщениеОКукиОтсутствует() {
        Assert.assertFalse(homePage.checkCookiePresented());
    }

    @Когда("Обновить страницу")
    public void обновитьСтраницу() {
        driver.get(driver.getCurrentUrl());
    }

    @Когда("^навести курсор на \"([^\"]*)\"$")
    public void навестиКурсорНаЭлемент(String title) {
        this.titleOfCollection = title;
        homePage.moveCoursorToElement(title);
    }

    @Тогда("появляется список, разделенный на столбцы до {int} элементов")
    public void появляетсяСписокРазделенныйНаСтолбцыДоЭлементов(int arg0) {
        homePage.checkElementsInBrandsColumnsNotGreaterThanMax(titleOfCollection, arg0);
    }

    @Тогда("^появляется список с товарами для$")
    public void появляетсяСписокСТоварамиДляТипПотребителя(List<String> args) {
        homePage.checkDataContainsInSubNavigationSelection(titleOfCollection, args);
    }

    @Тогда("^появляется кнопки \"([^\"]*)\"$")
    public void появляетсяКнопкиТипКнопки(String buttonTitle) {
        homePage.checkButtonBrowseAllExist(titleOfCollection, buttonTitle);
    }

    @Когда("элементов в столбце больше {int}")
    public void элементовВСтолбцеБольше(int arg0) {
/*        listOfElements = homePage.getSubTitlesOfElementsBiggerThenArg0(titleOfCollection, arg0);*/
        listOfSubTitles = homePage.getListOfSubtitleText(titleOfCollection, arg0);
    }

    @Тогда("появляется кнопка {string}")
    public void появляетсяКнопка(String buttonTitle) {
        homePage.checkButtonBrowseAllExist(titleOfCollection, listOfSubTitles, buttonTitle);
    }
}
