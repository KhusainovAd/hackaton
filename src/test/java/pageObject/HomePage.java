package pageObject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import qaHelp.BannersAndCarouselItemsTestData;
import qaHelp.InitHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class HomePage extends Page {
    private static Logger logger = Logger.getLogger(HomePage.class.getName());
    private static String TITLEOFPAGE = "Apparel Site UK | Homepage";
    String title;
    ArrayList<String> listOfBunners;
    ArrayList<String> listOfCarouselItems;

    @FindBy(xpath = "//div[@id='js-cookie-notification']/button")
    private WebElement cookieButton;

    @FindBy(xpath = "//div[@id='js-cookie-notification']")
    private WebElement cookieMsg;


    public HomePage(){
        logger.info("Открываю главную страницу:" + System.getProperty("mainpage"));
        PageFactory.initElements(driver, this);
        driver.get(System.getProperty("mainpage"));
        title = driver.getTitle();
        Assert.assertEquals(TITLEOFPAGE, title);
    }

    public void getTitlesOfBunnersOnMainPage() {
        logger.info("Достаем названия (titles) баннеров на главной странице");
        listOfBunners = new ArrayList<>();
        List<WebElement> listOfElements = driver.findElements(By.xpath("//div[@class[contains(.,'simple-banner')]]//img"));
        for (WebElement elem : listOfElements) {
            listOfBunners.add(elem.getAttribute("title"));
        }
        logger.info("Список баннеров на главной странице:" + listOfBunners);
    }

    public void clickOnNextBannerCheckOpenedPageThenReturnToMainPage() {
        logger.info("Открываем следующий по списку баннер");
        WebElement currentElement;
        for (String bunnerTitle : listOfBunners) {
            logger.info("Открываем баннер:" + bunnerTitle);
            currentElement = driver.findElement(By.xpath("//div[@class[contains(.,'simple-banner')]]" +
                    "//img[@title = '"+bunnerTitle+"']"));
            InitHelper.innerWaitElement(currentElement);
            currentElement.click();
            if (bunnerTitle.equals("hybris Accelerator")) {
                Assert.assertEquals(TITLEOFPAGE, title);
            }
            else {
                String currentLiElementText = driver.findElement(By.xpath("(//ol[@class='breadcrumb']/li)[last()]")).getText();
                Assert.assertEquals(BannersAndCarouselItemsTestData.bannersMap.get(bunnerTitle).toUpperCase(), currentLiElementText);
            }
            logger.info("Возвращамеся на предыдущую (главную) страницу");
            driver.navigate().back();
            Assert.assertEquals(TITLEOFPAGE, title);
        }
    }

    public void getListOfCarouselItems() {
        logger.info("Достаем названия (titles) items на главной странице");
        listOfCarouselItems = new ArrayList<>();
        List<WebElement> listOfElements = driver.findElements(By.xpath("//div[@class[contains(.,'carousel__item')]]//img"));
        for (WebElement elem : listOfElements) {
            listOfCarouselItems.add(elem.getAttribute("title"));
        }
        logger.info("Список carousel items на главной странице:" + listOfCarouselItems);
    }

    public void clickOnNextCarouselItemCheckOpenedPageThenReturnToMainPage() {
        logger.info("Открываем следующий по списку предмет");
        WebElement currentElement;
        for (String carouselItemsTitle : listOfCarouselItems) {
            logger.info("Открываем баннер:" + carouselItemsTitle);
            currentElement = driver.findElement(By.xpath("//div[@class[contains(.,'carousel__item')]]" +
                    "//img[@title = '"+carouselItemsTitle+"']"));
            InitHelper.carouselHandler(listOfCarouselItems, currentElement);
            currentElement.click();
            String currentLiElementText = driver.findElement(By.xpath("(//ol[@class='breadcrumb']/li)[last()]")).getText();
            Assert.assertEquals(BannersAndCarouselItemsTestData.carouselItemsMap.get(carouselItemsTitle).toUpperCase(), currentLiElementText);
            logger.info("Возвращамеся на предыдущую (главную) страницу");
            driver.navigate().back();
            Assert.assertEquals(TITLEOFPAGE, title);
        }
    }

    public boolean checkCookiePresented() {
        String cookieText = "We use cookies to ensure that we give you the best experience on our website. If you continue, you agree with our policy statement.";
        if (driver.findElements(By.xpath("//div[@id='js-cookie-notification']")).size()>0)
        {
            Assert.assertTrue(cookieMsg.getText().contains(cookieText));
            logger.info("Cookie на главной");
            return true;
        }
        else {
            logger.info("Cookie на главной нет");
            return false;
        }

    }

    public void closeCookiesMsg() {
        logger.info("Закрываем cookie сообщение");
        cookieButton.click();
    }

    public void moveCoursorToElement(String title) {
        By by = By.xpath("//a[@title='"+title+"']");
        Actions action = new Actions(driver);
        WebElement elem = driver.findElement(by);
        action.moveToElement(elem);
        action.perform();
    }

    public void checkElementsInBrandsColumnsNotGreaterThanMax(String title, int max) {
        List<WebElement> listOfElements = driver.findElements(By.xpath("//a[@title='"+title+"']/../..//div[@class='row']/div"));
        for (int i = 1; i <= listOfElements.size(); i++) {
            List<WebElement> listOfColumnElements = driver.findElements(By.xpath("//a[@title='"+title+"']/../..//div[@class='row']/div["+i+"]/ul/li"));
            Assert.assertTrue(listOfColumnElements.size() <= max);
            logger.info("Элементов в этом столбце: "+ listOfColumnElements.size() + ",что не больше " + max);
        }
    }

    public List<WebElement> getElementsInCategoryTypes(String title) {
        List<WebElement> listOfElements = driver.findElements(By.xpath("//a[@title='" + title + "']/../..//div[@class='row']/div"));
        logger.info("Достали элементы находящиеся под: " + title);
        return listOfElements;
    }

    public void checkDataContainsInSubNavigationSelection(String title, List<String> args) {
        List<WebElement> listOfElements = getElementsInCategoryTypes(title);
        for (int i = 0; i < listOfElements.size(); i ++) {
            int y = i+1;
            WebElement element =  driver.findElement(By.xpath("//a[@title='"+title+"']/../..//div[@class='row']/div["+y+"]/div"));
            InitHelper.innerWaitElement(element);
            String text = element.getText();
            logger.info("Проверям, что в категории товаров: " + title + " лежит спсиок с товарами для: " + args.get(i)) ;
            Assert.assertEquals(args.get(i).toUpperCase(), text);
        }
    }

    public void checkButtonBrowseAllExist(String title, String buttonTitle) {
        List<WebElement> listOfElements = getElementsInCategoryTypes(title);
        for (int i = 0; i < listOfElements.size(); i ++) {
            int y = i + 1;
            WebElement lastLiInDiv =  driver.findElement(By.xpath("//a[@title='"+title+"']" +
                    "/../..//div[@class='row']/div["+y+"]/ul/li[last()]/a"));
            Assert.assertTrue(lastLiInDiv.getText().contains(buttonTitle.toUpperCase()));
            logger.info("Последний элемент в столбце: " + driver.findElement(By.xpath("//a[@title='"+title+"']" +
                    "/../..//div[@class='row']/div["+y+"]/div")).getText() + " является кнопкой: " + buttonTitle);
        }
    }

    public void checkButtonBrowseAllExist(String titleOfCollection, List<String> listOfSubTitles, String buttonTitle) {
        if (listOfSubTitles.size()>0) {
        for (int i = 0; i < listOfSubTitles.size(); i ++) {
            int y = i + 1;
            String searchingTitle = listOfSubTitles.get(i);
            WebElement lastLiInDiv = driver.findElement(By.xpath("//a[@title='" + titleOfCollection + "']/../..//div[@class='row']" +
                    "/div/div//text()[contains(translate(., 'EST', 'est'), '" + searchingTitle.toLowerCase() + "')]/../../ul/li[last()]/a"));
            Assert.assertTrue(lastLiInDiv.getText().contains(buttonTitle.toUpperCase()));
            logger.info("Последний элемент в столбце: " + driver.findElement(By.xpath("//a[@title='" + titleOfCollection + "']" +
                    "/../..//div[@class='row']/div[" + y + "]/div")).getText() + " является кнопкой: " + buttonTitle);
        }
        }
        else {
        }
    }

    public  List<WebElement> getSubTitlesOfElementsBiggerThenArg0(String titleOfCollection, int arg0) {
        List<WebElement> listOfElements = getElementsInCategoryTypes(titleOfCollection);
        List<WebElement> listofNewElements = null;
        for (int i = 0; i < listOfElements.size(); i++) {
            int y = i + 1;
            List<WebElement> listOfSubElements = driver.findElements(By.xpath("//a[@title='" + titleOfCollection + "']/../..//div[@class='row']/div[" + y + "]/div/ul/li"));
            if (listOfSubElements.size() > arg0)
                listofNewElements.add(driver.findElement
                        (By.xpath("//a[@title='" + titleOfCollection + "']/../..//div[@class='row']/div[" + y + "]")));
        }
        return listofNewElements;
    }

    public ArrayList<String> getListOfSubtitleText(String titleOfCollection, int arg0) {
        List<WebElement> listOfElements = getElementsInCategoryTypes(titleOfCollection);
        ArrayList<String> listOfSubTitleText = new ArrayList<>();
        for (int i = 0; i < listOfElements.size(); i++) {
            int y = i + 1;
            List<WebElement> listOfSubElements = driver.findElements(By.xpath("//a[@title='" + titleOfCollection + "']/../..//div[@class='row']/div[" + y + "]/ul/li"));
            if (listOfSubElements.size() > arg0) {
                WebElement element = driver.findElement(By.xpath("//a[@title='" + titleOfCollection + "']/../..//div[@class='row']/div[" + y + "]/div"));
                listOfSubTitleText.add(element.getText());
            }
        }
        return listOfSubTitleText;
    }
}
