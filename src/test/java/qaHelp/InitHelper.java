package qaHelp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.hackaton.DriverFactory;

import java.util.ArrayList;
import java.util.logging.Logger;

public class InitHelper extends DriverFactory {
    private static Logger logger = Logger.getLogger(InitHelper.class.getName());
    public static void innerWaitElement (WebElement element) {
    try {
        WebDriverWait wait = new WebDriverWait(driver, 1);
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    catch (TimeoutException e) {
            throw new TimeoutException("Не нашел элемент: " + element + "на странице: " + driver.getTitle());
        }
    }

    public static void carouselHandler(ArrayList<String> listOfCarouselItems, WebElement element) {
        for (int i=0; i < listOfCarouselItems.size(); i++) {
            if (!element.isDisplayed()) {
                WebDriverWait wait = new WebDriverWait(driver, 10);
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].scrollIntoView(true);", element);
/*                WebElement elementOwl = wait.until(ExpectedConditions.
                        visibilityOf(driver.findElement(By.xpath("//div[@class='owl-next']"))));*/
                WebElement elementOwl = driver.findElement(By.xpath("//div[@class='owl-next']"));
                executor.executeScript("arguments[0].click();", elementOwl);
            }
        }
    }

}
