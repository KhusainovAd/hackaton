import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import javax.security.auth.login.Configuration;

public class HelloWorld {
    WebDriver driver;

    @Before
    public void beforeTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, Configuration.getConfiguration());
        caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
        caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        caps.setCapability("ignoreProtectedModeSettings", true);
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
    }

    @Test
    public void googleTest() {
        //System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver.exe");
        //System.setProperty("webdriver.ie.driver", "src/drivers/IEDriverServerx64.exe");
        //driver = new Int;
        driver.get(System.getProperty("mainpage"));
         driver.quit();
    }

    @After
    public void afterTest() {
        //driver.close();
    //    driver.quit();
    }
}
